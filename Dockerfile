FROM ruby:2.5-alpine

RUN apk update && apk add build-base nodejs postgresql-dev tzdata

RUN mkdir /terrace
WORKDIR /terrace

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

CMD ./bin/setup
