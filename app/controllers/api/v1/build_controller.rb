# frozen_string_literal: true

# :nodoc:
module Api
  module V1
    class BuildController < ApplicationController
      helper ::Api::V1::ApiHelper
      def create
        @thing = { errors: [] }
        if params[:payload].nil?
          @thing[:errors] <<
            {
              "status": :not_found,
              "code":  "no_payload",
              "title": "No payload found to build",
              "detail": "The user has not provided a payload in the request"
            }
          status  = :not_found
        elsif helpers.valid_json?(params[:payload].to_s)
          message = 'Valid payload found'
          status  = :ok
        else
          @thing[:errors] <<
            {
              "status": :unprocessable_entity,
              "code":  "invalid_payload",
              "title": "Invalid json in payload",
              "detail": "The user has provided a payload that contains invalid JSON"
            }
          status = :unprocessable_entity
        end
        @thing[:message] = message
        render status: status
      end
    end
  end
end
