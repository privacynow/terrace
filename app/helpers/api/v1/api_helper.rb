# :nodoc:
module Api
  module V1
    module ApiHelper
      def valid_json?(json)
        JSON.parse(json)
        return true
      rescue JSON::ParserError => e
        return false
      end
    end
  end
end