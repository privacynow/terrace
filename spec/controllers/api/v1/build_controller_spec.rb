require "rails_helper"

describe Api::V1::BuildController, type: :controller do
  render_views

  context 'when the payload does not exist' do
    before do
      post "/api/v1/build", params: {}, format: :json
    end

    it 'responds with a 404 status' do
      expect(status).to eq 404
    end

    it 'responds with a message of No payload to process' do
      message = json['errors'].first['detail']
      expect(message).to eq('The user has not provided a payload in the request')
    end
  end

  context 'when the payload contains invalid json' do
    before do
      post "/api/v1/build", payload: "{fasdfff{dsafd{", format: :json
    end

    it 'responds with a 422 status' do
      expect(status).to eq 422
    end

    it 'responds with a message of Invalid json in payload' do
      message = json['errors'].first['detail']
      expect(message).to eq('The user has provided a payload that contains invalid JSON')
    end
  end

  context 'when the payload is valid' do
    before do
      post "/api/v1/build", payload: {}.to_json, format: :json
    end

    it 'responds with a 200 status' do
      expect(status).to eq 200
    end

    it 'responds with a message of Valid payload found' do
      message = json['message']
      expect(message).to eq('Valid payload found')
    end
  end
end