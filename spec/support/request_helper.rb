module Requests
  module JsonHelpers
    def json
      JSON.parse(last_response.body)
    end

    def status
      last_response.status
    end
  end
end